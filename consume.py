from re import split
import re
import os

import pandas as pd

from helpers import *

def process_unit(frags):
    unit = ""
    for frag in frags:
        frag = frag.upper()
        if frag in unit_indicators.keys():
            unit = unit + ' ' + unit_indicators[frag]
        ## keep any fragment that has a numeral in it
        elif bool(re.search(r'\d', frag)):
                frag = frag.replace("#","")
                frag = frag.replace("APT","")
                unit = unit + ' ' + frag
        else: 
            ## todo catch anything like FIRST, SECOND, etc.
            #print("UNIT SKIPPED",frag)
            pass
    return unit.replace("-","").strip()


def process_street(frags):
    keep = []
    for frag in frags:
        if frag.replace("/","").upper()=='AKA':
            return " ".join(keep)
        else:
            keep.append(frag)
    return " ".join(keep)

def process_street_number(sn):
    multi = sn.split("-")
    if len(multi)==1:
        return multi[0].strip()
    elif len(multi)>2:
        print("ANOMALOUS STREET NUMBER",sn)
        return sn.strip()
    else:
        try:
            interval = int(multi[1]) - int(multi[0])
        except ValueError:
            print("INVALID INTEGERS",multi[0],multi[1])
            return "-".join(multi)
        if interval%2==0 and interval>0:
            return multi[0].strip()
        else:
            return "".join(multi)

def process_city(city):
    return city.strip()

def process_state(state):
    state = state.strip()
    if len(state)!=2:
        print("ANOMALY STATE",state)
    return state

def process_zip(zip):
    zip = zip.split("-")[0].strip()
    return zip

def extract_street_and_unit(frags):
    #print('\n' + "EXTRACT",frags)
    #frags = [f.replace("/","") for f in frags]
    frags = [frag.upper() for frag in frags]

    street_frags = []
    unit_frags = []
    breaks = []
    matched = []

    ## loop over al fragments and identify those that are special street identifiers
    ## store these in matched and their corresponding indexes in breaks
    for i in range(len(frags)):
        frag  = frags[i]
        if frag in addr_suffixes.keys():
            breaks.append(i)
            matched.append(frag)
            ## normalize the matched fragment based on our dictionary
            frags[i] = addr_suffixes[frag]

    ## if we failed to find any breaks, just return everything as street and nothing for unit
    if len(breaks)==0:
        print("FAILED TO SPLIT"," ".join(frags))
        return frags, []

    idx = breaks[0] + 1

    ## trivial case where there's only one break; split street and unit accordingly
    if len(breaks)==1:
        street_frags = frags[:idx]
        unit_frags = frags[idx:]
        #print("simple break"," ".join(street_frags),"<<>>"," ".join(unit_frags))
    
    ## multiple breaks; handle special cases and by default
    else:
        ## default strategy is to use the last identified break to split street and unit
        ## unless we find some special case

        idx = breaks[-1] + 1
        u_idx =  idx
        u_term = len(frags)

        ## this is meant to handle an ambigous situation like
        ## LENOX AVENUE C
        ## the algorithm needs to interpret C in this case as a unit number not street name
        if matched[-1] in unit_indicators \
        and (breaks[0]!=0 or len(breaks)>2) \
        and len(breaks)<len(frags):
            #print("    SPECIAL:", matched, ">>", matched[-1],"<<")
            idx = breaks[-2] + 1
            u_idx =  idx

        ## if the address has an AKA clause try to work around it
        ## the trick here is to determine if the AKA  relates to street or unit
        if 'AKA' in matched:
            #print("    AKA:",matched)
            if matched[-1]=='AKA':
                idx = breaks[-2] + 1
                u_idx = idx
                u_term = breaks[-1]
            else:
                idx = breaks[matched.index('AKA') - 1] + 1
                u_idx =  idx
                u_term = breaks[matched.index('AKA')]
            #print("=+=+= complex break",breaks, matched,idx)
        street_frags = frags[:idx]
        unit_frags = frags[u_idx:u_term]
    
    ## if we have many breaks we may need to handle some other special cases...
    if len(matched)>3:
        #print("    ",len(matched), "BREAKS:",breaks, matched, " ".join(frags))
        pass

    #print("    RESULT:",breaks, street_frags, unit_frags)
    return street_frags, unit_frags

def parse_address(addr):
    ## store the original address string
    final = {'original':'"""' + addr.replace('"',"") + '"""'}
    process = {}

    ## take out extraneous punctuation
    addr = addr.replace(".","")
    addr = addr.replace("#","")
    addr = addr.replace("/","")
    addr = addr.replace('"','')
    addr = addr.replace('- ','-')
    addr = addr.replace(' -','-')

    ## handle NaN arguments
    if type(addr)==float:
        print(i,"INVALID ADDRESS",addr)
        return  final

    else:                           
        frags = []
        try:
            ## by default split on commas
            frags = addr.split(",")
        except AttributeError:
            print("Unable to split",addr)

        ## ideal format: comma-separated fragments of street, unit, city, state, zip
        ## exclude AKA addresses from this case
        if len(frags)==5 and ('AKA' not in addr.split()):
            process['street_number'] = frags[0].split()[0]
            process['street'] = frags[0].split()[1:]
            process['unit'] = frags[1].split()
            process['city'] = frags[2].strip()
            process['state'] = frags[3].strip()
            process['zip'] = frags[4].strip()

        ## otherwise fall back to the generic strategy
        else:
            if len(frags)<4:
                ## need to split on spaces instead, just forget about the commas
                frags = addr.replace(",","").split()

            ## we assume the last three fragments are city, state, zip
            split_point = len(frags) - 3

            ## everything else we split on spaces and parse
            frags = " ".join(frags[0:split_point]).replace(","," ").split() + frags[split_point:]

            process['street_number'] = frags[0]
            process['zip'] = frags[-1].strip()
            process['state'] = frags[-2].strip()
            process['city'] = frags[-3].strip()
            process['street'] = frags[1:-3]

            ## parse all fragments to break out street and unit information
            process['street'], process['unit'] = extract_street_and_unit(process['street'])

        ## final normalizatin/ formatting of each address element
        final['street_number'] = process_street_number(process['street_number'])
        final['city'] = process_city(process['city'])
        final['state'] = process_state(process['state'])
        final['zip'] = process_zip(process['zip'])
        final['unit'] = process_unit(process['unit'])
        final['street'] = process_street(process['street'])

    return final

def parse_address_set(df, addr_col, output_cols=['original','street_number','street','unit','city','state','zip'], outputfilename="parsed.csv"):
    carry_cols = [col for col in df.columns]
    all_cols = output_cols + carry_cols
    with open(outputfilename, 'w') as f:
        f.write(",".join(all_cols) + '\n')
        for i in df.index:
            res = parse_address(df.loc[i,addr_col])
            carry_columns_string = ''
            for col in carry_cols:
                carry_columns_string = carry_columns_string + ',' + '"' + str(df.loc[i,col]) + '"'
            f.write(",".join(res[col] for col in output_cols) + carry_columns_string + '\n')
    print("wrote",outputfilename)


if __name__=="__main__":
    df = pd.read_excel("hardones.xlsx")
    parse_address_set(df, 'address')
